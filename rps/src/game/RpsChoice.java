package game;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
/**
 * 
 * @author Juan-Carlos Sreng-Flores
 *
 */
public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message, wins, ties, losses;
	private String playerChoice;
	private RpsGame game;
	public RpsChoice(TextField message,TextField wins,TextField ties,TextField losses,String playerChoice, RpsGame game) {
		this.message = message;
		this.wins = wins;
		this.ties = ties;
		this.losses = losses;
		this.playerChoice = playerChoice;
		this.game = game;	
	}
	@Override
	public void handle(ActionEvent choice) {
		String result = game.playRound(playerChoice);
		message.setText(result);
		wins.setText("Wins: "+game.getWins());
		ties.setText("Ties: "+game.getTies());
		losses.setText("Losses: "+game.getLosses());
	}
}
