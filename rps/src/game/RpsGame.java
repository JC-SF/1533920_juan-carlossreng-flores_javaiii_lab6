/**
 * @author Juan-Carlos Sreng-Flores
 */
package game;
import java.util.*;
public class RpsGame {
	private int wins;
	private int ties;
	private int losses;
	private String[] choice;
	private Random rand;
	public RpsGame() {
		wins = 0;
		ties = 0;
		losses = 0;
		choice = new String[] {"rock", "paper", "scissors"};
		rand = new Random();
	}
	public int getWins() {
		return wins;
	}
	public int getTies() {
		return ties;
	}
	public int getLosses(){
		return losses;
	}
	public String playRound(String playerChoice) {
		int pChoice = -1;
		boolean isValid = false;
		for(int i = 0; i<choice.length; i++) {
			if(playerChoice.equalsIgnoreCase(choice[i])) {
				pChoice = i;
				isValid = true;
				break;
			}
		}
		if(isValid) {
			int compChoice = rand.nextInt(choice.length);
			String round;
			if(pChoice == compChoice) {
				round = "It's a Tie!";
				ties++;
			}
			else if(pChoice == (compChoice-1)%choice.length) {
				//Player loses.
				round = "Bot's hand wins!";
				losses++;
			}
			else {
				//Player wins
				round = "Player's hand wins!";
				wins++;
			}
			return round;
		}
		else {
			throw new IllegalArgumentException("Player's hand is not valid! Inserted: "+playerChoice);
		}
	}
	public static void main(String[] args) {
		boolean isOver = false;
		java.util.Scanner scan = new java.util.Scanner(System.in);
		String[] choice = new String[] {"rock", "paper", "scissors"};
		RpsGame rps = new RpsGame();
		while(!isOver) {
			int n = scan.nextInt();
			if(n == -1) {
				break;
			}
			String p = choice[n];
			System.out.println(rps.playRound(p));
		}
	}
}