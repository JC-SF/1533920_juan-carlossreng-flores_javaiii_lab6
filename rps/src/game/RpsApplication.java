/**
 * @author Juan-Carlos Sreng-Flores
 */
package game;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
public class RpsApplication extends Application {	
	RpsGame rps = new RpsGame();
	public void start(Stage stage) {
		final String ROCK = "rock";
		final String PAPER = "paper";
		final String SCISSORS = "scissors";
		Group root = new Group(); 
		Button rock = new Button(ROCK);
		Button paper = new Button(PAPER);
		Button scissors = new Button(SCISSORS);
		TextField welcome, wins, ties, losses;
		welcome = new TextField("Welcome");
		wins = new TextField("Wins: ");
		ties = new TextField("Ties: ");
		losses = new TextField("Losses: ");
		//Setting up buttons
		HBox buttons = new HBox();
		buttons.getChildren().addAll(rock, paper, scissors);
		//Setting up textfield.
		HBox textfields = new HBox();
		textfields.getChildren().addAll(welcome, wins,ties,losses);
		//Setting up vertical buttons on top of text.
		VBox v = new VBox();
		v.getChildren().addAll(buttons,textfields);
		root.getChildren().add(v);
		
		//Set up events
		RpsChoice action = new RpsChoice(welcome, wins, ties, losses, ROCK, rps);
		rock.setOnAction(action);
		action = new RpsChoice(welcome, wins, ties, losses, PAPER, rps);
		paper.setOnAction(action);
		action = new RpsChoice(welcome, wins, ties, losses, SCISSORS, rps);
		scissors.setOnAction(action);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
    public static void main(String[] args) {
        Application.launch(args);
    }
} 